#include<bits/stdc++.h>
using namespace std;

double fn(double x)
{
    return sin(x*x) ;
}
 
void bisection(double a, double b)
{
     double EPSILON;
     cout << "Enter EPSILON" << endl;
     cin >> EPSILON;

    if (fn(a) * fn(b) >= 0)
    {
        cout << "You have not assumed right a and b\n";
        return;
    }
 
    double c = a;
    while ((b-a) >= EPSILON)
    {
         c = (a+b)/2;
 
        // Check if c is root
        if (fn(c) == 0.0)
            break;
 
        // Or repeat the steps
        else if (fn(c)*fn(a) < 0)
            b = c;
        else
            a = c;
    }
    cout << "The value of root is : " << c;
    //continue;
}
 
// Take input for above function
int main()
{
    // Take input
    cout << "Enter ´a´" << endl;
    cout << "Enter ´b´" << endl;
    double a, b;
    cin >> a;
    cin >> b;
    if (cin.good()) {
        bisection(a, b);
        return 0;
    }
    if (cin.fail()) {
        cout << "Enter correct inputs" << endl;
        //break;
    }
    //bisection(a, b);
    //return 0;
}