#include<iostream>
#include<conio.h>
#include<math.h>

using namespace std;

int main()
{
 float x[100], y[100];
 int n;
 float p, ans;
 
 //Inputs
 
 cout << "Enter number of data: " << endl;
	 cin >> n;
	 cout << "Enter data: " << endl;
	 for( int i = 1 ; i <= n ; i++ )
	 {
		  cout << "x["<< i<<"] = ";
		  cin >> x[i];
		  y[i] = sin( x[i] * x[i] );
	 }
	 cout << "Enter interpolation point: ";
	 cin >> p;


 // Linear Interpolation 
 
        for ( int i = 1 ; i <= n ; i++)
        {
            ans = y[i] + ( ( y[i+1] - y[i] ) / ( x[i+1] - x[i] ) ) * (p - x[i]);

            cout << "Interpolation between " << x[i] << " and " << y[i] << endl;
            cout << "Interpolated value at "<< p <<" is "<< ans << endl;
            cout << "" << endl;
        }
        

 return 0;
}